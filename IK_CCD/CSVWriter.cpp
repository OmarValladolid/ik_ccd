#include <iostream>
#include <fstream>
#include "CSVWriter.h"
#include <string>

CSVWriter::CSVWriter(string fileName, string delimiter = ",")
{
	_fileName = fileName;
	_delimiter = delimiter;
	_linesCount = 0;
}

CSVWriter::~CSVWriter() {}

void CSVWriter::WriteHeader()
{
	vector<string> header = {"Frame#", "TargetX", "TargetY", "Body#", "EffVec_X", "EffVec_Y", "TVec_X", "TecV_Y", "Angle", "NANMatrix", "Body#Mod", "SOPos_X", "SOPos_Y", "EOPos_X", "EOpos_Y", "SNewPos_X", "SNewPos_Y", "ENewPos_X", "ENewPos_Y"};
	this->WriteLine(header.begin(), header.end());
}

template<typename T>
void CSVWriter::WriteLine(T first, T last)
{
	fstream file;
	file.open(_fileName, ios::out | (_linesCount ? ios::app : ios::trunc));

	if (file)
	{
		for (; first != last;)
		{
			file << *first;
			if (++first != last)
				file << _delimiter;
		}

		file << "\n";
		_linesCount++;

		file.close();
	}
}