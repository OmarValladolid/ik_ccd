#include "Point2D.h"

/// <summary>
/// Point2D constructor
/// </summary>
Point2D::Point2D(void)
{
	SetX(0);
	SetY(0);
}

/// <summary>
/// Point2D constructor
/// </summary>
/// <param name="x">x value</param>
/// <param name="y">y value</param>
Point2D::Point2D(float x, float y)
{
	SetX(x);
	SetY(y);
}

/// <summary>
/// Point2D destructor
/// </summary>
Point2D::~Point2D()
{
}

/// <summary>
/// Set the value of x
/// </summary>
/// <param name="x">x value</param>
void Point2D::SetX(float x)
{
	_x = x;
}

/// <summary>
/// Set the value of y
/// </summary>
/// <param name="x">y value</param>
void Point2D::SetY(float y)
{
	_y = y;
}

/// <summary>
/// Set the value of x and y
/// </summary>
/// <param name="points">Point in 2D</param>
void Point2D::SetPoints(Point2D points)
{
	_x = points.GetX();
	_y = points.GetY();
}

/// <summary>
/// Get the value of x
/// </summary>
/// <returns>x value</returns>
float Point2D::GetX(void)
{
	return _x;
}

/// <summary>
/// Get the value of y
/// </summary>
/// <returns>y value</returns>
float Point2D::GetY(void)
{
	return _y;
}