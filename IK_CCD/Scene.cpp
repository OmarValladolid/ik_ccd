#include "Scene.h"
#include "GL\freeglut.h"

/// <summary>
/// Scene constructor
/// </summary>
/// <param name="left">Min value in x</param>
/// <param name="right">Max value in x</param>
/// <param name="top">Max value in y</param>
/// <param name="bottom">Min value in y</param>
/// <param name="near_">Max value in z</param>
/// <param name="far_">Min value in z</param>
Scene::Scene(float left, float right, float top, float bottom, float near_, float far_)
{
	_left = left;
	_right = right;
	_top = top;
	_bottom = bottom;
	_near = near_;
	_far = far_;
}

/// <summary>
/// Scene destructor
/// </summary>
Scene::~Scene()
{}

/// <summary>
/// Method that draw the main axes in the coordinate system
/// </summary>
void Scene::DrawAxes(void)
{
	glBegin(GL_LINES);
	glVertex3f(_left, 0, 0);
	glVertex3f(_right, 0, 0);

	glVertex3f(0, _top, 0);
	glVertex3f(0, _bottom, 0);
	glEnd();
}

/// <summary>
/// Draws the grid in the coordinate system along the x and y axes
/// </summary>
void Scene::DrawGrid(void)
{
	glBegin(GL_LINES);
	for (float i = _left; i <= _right; i = i + 10)
	{
		glVertex3f(i, _top, 0);
		glVertex3f(i, _bottom, 0);
	}

	for (float i = _bottom; i <= _top; i = i + 10)
	{
		glVertex3f(_left, i, 0);
		glVertex3f(_right, i, 0);
	}
	glEnd();
}