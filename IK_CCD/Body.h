#pragma once
#include "LineObj.h"
#include "Point2D.h"

/// <summary>
/// Body class
/// </summary>
class Body
{
private:
	Point2D _points[4];

	/// <summary>
	/// Load bodies defined.
	/// You can modified the vertex in this method
	/// </summary>
	void _LoadPoints(void);

public:
	LineObj bodies[3];
	int size;

	/// <summary>
	/// Body constructor
	/// </summary>
	Body(void);

	/// <summary>
	/// Body destructor
	/// </summary>
	~Body();

	/// <summary>
	/// Load bodies defined.
	/// </summary>
	void LoadBodies(void);

	/// <summary>
	/// Draw bodies
	/// </summary>
	void DrawBodies(void);
};