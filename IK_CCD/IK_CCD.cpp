// IK_CCD.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "pch.h"
#include <iostream>
#include <sstream>
#include "GL\freeglut.h"
#include "Body.h"
#include "Point2D.h"
#include "Space2D.h"
#include "Scene.h"
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <vector>
#include "CSVWriter.h"

// Variables
static int screenWidth = 800, screenHeight = 800;
static GLdouble cs_left = -200, cs_right = 200, cs_top = 200, cs_bottom = -200;
static int frameNumber = 1;
int	mouse_x = 0, mouse_y = 0;
Space2D space2D;
Body newBody;
Scene newScene(-200, 200, 200, -200, 50, -50);

Point2D target(-70,-61);

CSVWriter fileWriter("CDD_Log.csv", ",");

GLfloat spin = 0.0;
GLfloat spin_incr = 1.0;

bool matrixNan = false;
bool isMouseLeftHeld = false;

// Text
GLvoid *font_style = GLUT_BITMAP_HELVETICA_12;

// OpenGL Function Prototypes
void init();
void display();
void reshape(int width, int height);
void update();
void mouseFunc(int button, int state, int x, int y);
void motionFunc(int x, int y);

// Other fuctions
void printText(void);
void printString(float x, float y, float z, const char* format, ...);

void init()
{
	// Background is set to Black
	glClearColor(0.0, 0.0, 0.0, 1.0);
	newBody.LoadBodies();
	fileWriter.WriteHeader();
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	printText();

	glColor4f(0.5, 0.5, 0.0, 1.0);
	glLineWidth(2.0);
	newScene.DrawAxes();
	glColor4f(0.5, 0.5, 0.0, 0.5);
	glLineWidth(1.0);
	newScene.DrawGrid();

	// Center of world
	glColor3f(0.0, 0.0, 1.0);
	glBegin(GL_POINTS);
	glVertex2f(0.0, 0.0);
	glEnd();


	//glLineWidth(1.0);
	// Arm Base
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);
	glVertex2f(-14, -100);
	glVertex2f(-14, -86);
	glVertex2f(14, -86);
	glVertex2f(14, -100);
	glEnd();

	// Arm base center
	glColor3f(1.0, 0.0, 0.0);
	glPointSize(10.0);
	glBegin(GL_POINTS);
	glVertex2f(0, -93);
	glEnd();

	glLineWidth(5.0);

	// Target Coordinates
	glBegin(GL_POINTS);
		glVertex2f(target.GetX(), target.GetY());
	glEnd();

	
	newBody.DrawBodies();

	// End Effector Vector
	float u[2];
	// Target vector
	float v[2];
	// Result vector
	float r[2];

	// 
	float angle = 0;
	float sign = 0;
	const float epsilon = 0.0001;

	float newSX = 0;
	float newSY = 0;
	float newEX = 0;
	float newEY = 0;

	std::vector<std::string> textLine;
	
	do
	{
		for (int i = newBody.size - 1; i >= 0; i--)
		{
			// End Effector Vector
			// The origin is the current link to evaluate but the end is the end of the max link in the body
			u[0] = newBody.bodies[newBody.size - 1].GetEnd().GetX() - newBody.bodies[i].GetStart().GetX();
			u[1] = newBody.bodies[newBody.size - 1].GetEnd().GetY() - newBody.bodies[i].GetStart().GetY();

			// Target Vector
			v[0] = target.GetX() - newBody.bodies[i].GetStart().GetX();
			v[1] = target.GetY() - newBody.bodies[i].GetStart().GetY();

			angle = space2D.AngleBetweenVectors(u, v);
			sign = space2D.CrossProduct(u, v);

			// Remove the sign of angle
			angle = sqrt(angle * angle);

			if (sign < 0) angle = angle * -1;

			glPushMatrix();
			glTranslatef(newBody.bodies[i].GetStart().GetX(), newBody.bodies[i].GetStart().GetY(), 0.0);
			glRotatef(angle, 0.0, 0.0, 1.0);
			glTranslatef((-1)*newBody.bodies[i].GetStart().GetX(), (-1)*newBody.bodies[i].GetStart().GetY(), 0.0);

			for (int j = i; j <= newBody.size - 1; j++)
			{
				glColor3f(1.0, 1.0, 0.0);
				glBegin(GL_POINTS);
				glColor3f(0.0, 0.5, 0.0);
				glVertex2f(newBody.bodies[j].GetEnd().GetX(), newBody.bodies[j].GetEnd().GetY());
				glColor3f(0.0, 1.0, 0.0);
				glVertex2f(newBody.bodies[j].GetStart().GetX(), newBody.bodies[j].GetStart().GetY());
				glEnd();

				glBegin(GL_LINES);
				glVertex2f(newBody.bodies[j].GetStart().GetX(), newBody.bodies[j].GetStart().GetY());
				glVertex2f(newBody.bodies[j].GetEnd().GetX(), newBody.bodies[j].GetEnd().GetY());
				glEnd();
			}
			GLfloat mat[16];
			glGetFloatv(GL_MODELVIEW_MATRIX, mat);
			glPopMatrix();

			if (!isnan(mat[0]))
			{
				for (int j = i; j <= newBody.size - 1; j++)
				{
					float start[3];
					float end[3];

					// Get new start and end vertices (Model View Matrix)
					start[0] = mat[0] * newBody.bodies[j].GetStart().GetX() + mat[4] * newBody.bodies[j].GetStart().GetY() + mat[8] * 1 + mat[12];
					start[1] = mat[1] * newBody.bodies[j].GetStart().GetX() + mat[5] * newBody.bodies[j].GetStart().GetY() + mat[9] * 1 + mat[13];
					start[2] = mat[2] * newBody.bodies[j].GetStart().GetX() + mat[6] * newBody.bodies[j].GetStart().GetY() + mat[10] * 1 + mat[14];

					end[0] = mat[0] * newBody.bodies[j].GetEnd().GetX() + mat[4] * newBody.bodies[j].GetEnd().GetY() + mat[8] * 1 + mat[12];
					end[1] = mat[1] * newBody.bodies[j].GetEnd().GetX() + mat[5] * newBody.bodies[j].GetEnd().GetY() + mat[9] * 1 + mat[13];
					end[2] = mat[2] * newBody.bodies[j].GetEnd().GetX() + mat[6] * newBody.bodies[j].GetEnd().GetY() + mat[10] * 1 + mat[14];

					Point2D newStart;
					Point2D newEnd;
					newStart.SetX(start[0]);
					newStart.SetY(start[1]);

					newEnd.SetX(end[0]);
					newEnd.SetY(end[1]);

					// Set the new values for the Start and End point
					newBody.bodies[j].SetStart(newStart);
					newBody.bodies[j].SetEnd(newEnd);

					glBegin(GL_POINTS);
					glColor3f(1.0, 0.5, 0.2);
					glVertex2f(start[0], start[1]);
					glVertex2f(end[0], end[1]);
					glEnd();

					// Write line in file
					textLine.clear();
					textLine.push_back(to_string(frameNumber));
					textLine.push_back(to_string(target.GetX()));
					textLine.push_back(to_string(target.GetY()));
					textLine.push_back(to_string(i));
					textLine.push_back(to_string(u[0]));
					textLine.push_back(to_string(u[1]));
					textLine.push_back(to_string(v[0]));
					textLine.push_back(to_string(v[1]));
					textLine.push_back(to_string(angle));
					textLine.push_back("false");

					textLine.push_back(to_string(j));
					textLine.push_back(to_string(newBody.bodies[j].GetStart().GetX()));
					textLine.push_back(to_string(newBody.bodies[j].GetStart().GetY()));
					textLine.push_back(to_string(newBody.bodies[j].GetEnd().GetX()));
					textLine.push_back(to_string(newBody.bodies[j].GetEnd().GetY()));
					textLine.push_back(to_string(newStart.GetX()));
					textLine.push_back(to_string(newStart.GetY()));
					textLine.push_back(to_string(newEnd.GetX()));
					textLine.push_back(to_string(newEnd.GetY()));
					fileWriter.WriteLine(textLine.begin(), textLine.end());
				}
				matrixNan = false;
			}
			else
			{
				// Write line in file
				textLine.clear();
				textLine.push_back(to_string(frameNumber));
				textLine.push_back(to_string(target.GetX()));
				textLine.push_back(to_string(target.GetY()));
				textLine.push_back(to_string(i));
				textLine.push_back(to_string(u[0]));
				textLine.push_back(to_string(u[1]));
				textLine.push_back(to_string(v[0]));
				textLine.push_back(to_string(v[1]));
				textLine.push_back(to_string(angle));
				textLine.push_back("true");
				fileWriter.WriteLine(textLine.begin(), textLine.end());
				matrixNan = true;
				break;
			}

			space2D.VectorSubstraction(u, v, r);
		}
	} while (space2D.Norm(r) <= epsilon);


	glFlush();
	glutSwapBuffers();

	frameNumber++;
}

void reshape(int width, int height)
{
	float aspectRatio = 0;

	screenWidth = width;
	screenHeight = height;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//gluOrtho2D(0, screenWidth, 0, screenHeight);

	if (width == 0) width = 1;
	if (height == 0) height = 1;

	if (width <= height)
	{
		aspectRatio = (float)width / (float)height;
		gluOrtho2D(cs_left, cs_right, cs_bottom * aspectRatio, cs_top * aspectRatio);
	}
	else
	{
		aspectRatio = (float)height/ (float)width;
		gluOrtho2D(cs_left * aspectRatio, cs_right * aspectRatio, cs_bottom, cs_top);
	}


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void update()
{
	glutPostRedisplay();
}

void mouseFunc(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		mouse_x = (x / 2) - 200;
		mouse_y = ((y / 2) - 200)*-1;

		target.SetX(mouse_x);
		target.SetY(mouse_y);
		isMouseLeftHeld = true;
	}

	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		isMouseLeftHeld = false;
}

void motionFunc(int x, int y)
{
	if (isMouseLeftHeld)
	{
		mouse_x = (x / 2) - 200;
		mouse_y = ((y / 2) - 200)*-1;

		target.SetX(mouse_x);
		target.SetY(mouse_y);
	}
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(screenWidth, screenHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Inverse Kinematics CCD Example");

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	init();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(update);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(motionFunc);
	glutMainLoop();

	return 0;
}

void printText(void)
{
	char buffer[100];

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	glMatrixMode(GL_MODELVIEW);

	memset(&buffer[0], 0, sizeof(buffer));

	glColor4f(0.5, 0.5, 0.5, 0.5);
	// First Smoke color rectangle
	glBegin(GL_POLYGON);
	glVertex3f(0.5, 98, 1);
	glVertex3f(35, 98, 1);
	glVertex3f(35, 64, 1);
	glVertex3f(0.5, 64, 1);
	glEnd();

	// Second Smoke rectangle
	glBegin(GL_POLYGON);
	glVertex3f(60, 98, 1);
	glVertex3f(85, 98, 1);
	glVertex3f(85, 80, 1);
	glVertex3f(60, 80, 1);
	glEnd();

	//Instructions
	glColor4f(1.0, 1.0, 1.0, 1.0);
	printString(1, 96, 1, "%s", "Body Data:");

	printString(10, 96, 1, "%s", "Bone0 Start - X:");
	_itoa_s(newBody.bodies[0].GetStart().GetX(), buffer, 10);
	printString(25, 96, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 94, 1, "%s", "Bone0 Start - Y:");
	_itoa_s(newBody.bodies[0].GetStart().GetY(), buffer, 10);
	printString(25, 94, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 92, 1, "%s", "Bone0 End - X:");
	_itoa_s(newBody.bodies[0].GetEnd().GetX(), buffer, 10);
	printString(25, 92, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 90, 1, "%s", "Bone0 End - Y:");
	_itoa_s(newBody.bodies[0].GetEnd().GetY(), buffer, 10);
	printString(25, 90, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 88, 1, "%s", "Bone1 Start - X:");
	_itoa_s(newBody.bodies[1].GetStart().GetX(), buffer, 10);
	printString(25, 88, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 86, 1, "%s", "Bone1 Start - Y:");
	_itoa_s(newBody.bodies[1].GetStart().GetY(), buffer, 10);
	printString(25, 86, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 84, 1, "%s", "Bone1 End - X:");
	_itoa_s(newBody.bodies[1].GetEnd().GetX(), buffer, 10);
	printString(25, 84, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 82, 1, "%s", "Bone1 End - Y:");
	_itoa_s(newBody.bodies[1].GetEnd().GetY(), buffer, 10);
	printString(25, 82, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 80, 1, "%s", "Bone2 Start - X:");
	_itoa_s(newBody.bodies[2].GetStart().GetX(), buffer, 10);
	printString(25, 80, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 78, 1, "%s", "Bone2 Start - Y:");
	_itoa_s(newBody.bodies[2].GetStart().GetY(), buffer, 10);
	printString(25, 78, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	
	printString(10, 76, 1, "%s", "Bone2 End - X:");
	_itoa_s(newBody.bodies[2].GetEnd().GetX(), buffer, 10);
	printString(25, 76, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 74, 1, "%s", "Bone2 End - Y:");
	_itoa_s(newBody.bodies[2].GetEnd().GetY(), buffer, 10);
	printString(25, 74, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));


	printString(10, 72, 1, "%s", "Target - X:");
	_itoa_s(target.GetX(), buffer, 10);
	printString(25, 72, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 70, 1, "%s", "Target - Y:");
	_itoa_s(target.GetY(), buffer, 10);
	printString(25, 70, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 68, 1, "%s", "Mouse - X:");
	_itoa_s(mouse_x, buffer, 10);
	printString(25, 68, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 66, 1, "%s", "Mouse - Y:");
	_itoa_s(mouse_y, buffer, 10);
	printString(25, 66, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	if(matrixNan)
		printString(60.5, 96, 1, "%s", "Model View Matrix is NAN: TRUE");
	else
		printString(60.5, 96, 1, "%s", "Model View Matrix is NAN: FALSE");


	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	//glDisable(GL_BLEND);
}

void printString(float x, float y, float z,const char* format, ...)
{
	va_list args;
	int len;
	int i;
	char * text;

	va_start(args, format);
	len = _vscprintf(format, args) + 1;
	text = (char*)malloc(len * sizeof(char));
	vsprintf_s(text, len, format, args);
	va_end(args);
	glRasterPos3f(x, y, z);
	for (i = 0; text[i] != '\0'; i++)
	{
		glutBitmapCharacter(font_style, text[i]);
	}
	free(text);

}