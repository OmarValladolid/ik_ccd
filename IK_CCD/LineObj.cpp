#include "LineObj.h"
#include "GL\freeglut.h"

/// <summary>
/// LineObj constructor
/// </summary>
LineObj::LineObj(void)
{
};

/// <summary>
/// LineObj destructor
/// </summary>
LineObj::~LineObj(void)
{
};

/// <summary>
/// LineObj constructor
/// </summary>
/// <param name="start">Start point</param>
/// <param name="end">End point</param>
/// <param name="lineColor">Line color</param>
/// <param name="pointColor">Point color for line vertices</param>
LineObj::LineObj(Point2D start, Point2D end, float lineColor[4], float pointColor[4])
{
	_start = start;
	_end = end;
	SetLineColor(lineColor);
	SetLineColor(pointColor);
}

/// <summary>
/// Set the start point
/// </summary>
void LineObj::SetStart(Point2D s)
{
	_start = s;
}

/// <summary>
/// Set the end point
/// </summary>
void LineObj::SetEnd(Point2D e)
{
	_end = e;
}

/// <summary>
/// Set the color of the line (RGBA)
/// </summary>
void LineObj::SetLineColor(float color[4])
{
	_lineColor[0] = color[0];
	_lineColor[1] = color[1];
	_lineColor[2] = color[2];
	_lineColor[3] = color[3];
}

/// <summary>
/// Set the color of the line vertices (RGBA)
/// </summary>
void LineObj::SetPointColor(float color[4])
{
	_pointColor[0] = color[0];
	_pointColor[1] = color[1];
	_pointColor[2] = color[2];
	_pointColor[3] = color[3];
}

/// <summary>
/// Set the color (RGBA) to be used in OpenGL
/// </summary>
void LineObj::SetColor(float color[4])
{
	glColor3f(color[0], color[1], color[2]);
}

/// <summary>
/// Get the start point
/// </summary>
/// <returns>Start point object</returns>
Point2D LineObj::GetStart(void)
{
	return _start;
}

/// <summary>
/// Get the end point
/// </summary>
/// <returns>End point object</returns>
Point2D LineObj::GetEnd(void)
{
	return _end;
}

/// <summary>
/// Draw line
/// </summary>
void LineObj::DrawLine()
{
	SetColor(_lineColor);
	glBegin(GL_LINES);
		glVertex2f(_start.GetX(), _start.GetY());
		glVertex2f(_end.GetX(), _end.GetY());
	glEnd();
}

/// <summary>
/// Draw line vertices
/// </summary>
void LineObj::DrawPoints(void)
{
	SetColor(_pointColor);
	glBegin(GL_POINTS);
		glVertex2f(_start.GetX(), _start.GetY());
		glVertex2f(_end.GetX(), _end.GetY());
	glEnd();
}