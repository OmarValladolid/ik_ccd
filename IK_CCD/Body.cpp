#include "Body.h"
#include "LineObj.h"
#include "Point2D.h"

/// <summary>
/// Body constructor
/// </summary>
Body::Body(void)
{
	Point2D _points[4];
	LineObj bodies[3];
	size = 3;
	_LoadPoints();
}

/// <summary>
/// Body destructor
/// </summary>
Body::~Body()
{}

/// <summary>
/// Load bodies defined.
/// You can modified the vertex in this method
/// </summary>
void Body::_LoadPoints(void)
{
	// Initial Points
	Point2D _pSLine1(0, -93);
	Point2D _pELine1(50, -20);
	Point2D _pELine2(10, 20);
	Point2D _pELine3(-20, 50);

	_points[0].SetPoints(_pSLine1);
	_points[1].SetPoints(_pELine1);
	_points[2].SetPoints(_pELine2);
	_points[3].SetPoints(_pELine3);
}

/// <summary>
/// Load bodies defined.
/// </summary>
void Body::LoadBodies(void)
{

	for (int i = 0; i < 3; i++)
	{
		// For all bodies except the root, the start vertex is the same as the end vertex in the previous body
		bodies[i].SetStart(_points[i]);
		bodies[i].SetEnd(_points[i + 1]);
	}
}

/// <summary>
/// Draw bodies
/// </summary>
void Body::DrawBodies(void)
{
	float lineColor[4] = { 0.70, 0.30, 0.30, 1.0 };
	float pointColor[4] = { 0.20, 0.0, 0.20, 1.0};

	for (int i = 0; i < 3; i++)
	{
		lineColor[0] += (float)0.1;
		lineColor[1] += (float)0.1;
		lineColor[2] += (float)0.2;

		pointColor[0] += (float)0.1;
		pointColor[1] += (float)0.2;
		pointColor[2] += (float)0.1;
		

		bodies[i].SetLineColor(lineColor);
		bodies[i].SetPointColor(pointColor);

		bodies[i].DrawLine();
		bodies[i].DrawPoints();
	}
}