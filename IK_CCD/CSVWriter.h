#pragma once
#include <iostream>
#include <vector>

using namespace std;

class CSVWriter
{
private:
	string _fileName;
	string _delimiter;
	int _linesCount;

public:
	CSVWriter(string fileName, string delimiter);
	~CSVWriter();
	void WriteHeader();
	template<typename T>
	void WriteLine(T first, T last);
};