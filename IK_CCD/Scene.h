#pragma once

/// <summary>
/// Scene class
/// </summary>
class Scene
{
private: float _left, _right, _top, _bottom, _near, _far;

public:
	/// <summary>
	/// Scene constructor
	/// </summary>
	/// <param name="left">Min value in x</param>
	/// <param name="right">Max value in x</param>
	/// <param name="top">Max value in y</param>
	/// <param name="bottom">Min value in y</param>
	/// <param name="near_">Max value in z</param>
	/// <param name="far_">Min value in z</param>
	Scene(float left, float right, float top, float bottom, float near, float far);
	
	/// <summary>
	/// Scene destructor
	/// </summary>
	~Scene();

	/// <summary>
	/// Draws the main axes in the coordinate system
	/// </summary>
	void DrawAxes(void);

	/// <summary>
	/// Draws the grid in the coordinate system along the x and y axes
	/// </summary>
	void DrawGrid(void);
};