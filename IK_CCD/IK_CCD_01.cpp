// IK_CCD.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "pch.h"
#include <iostream>
#include "GL\freeglut.h"
#include "Body.h"
#include "Point2D.h"
#include "Space2D.h"
#include "Scene.h"
#include <math.h>
#include <string.h>
#include <stdarg.h>

// Variables
static int screenWidth = 800, screenHeight = 800;
static int left = -200, right = 200, top = 200, bottom = -200;
static int frameNumber = 0;
Space2D space2D;
Body newBody;
Scene newScene(-200, 200, 200, -200, 50, -50);

Point2D target(-100,15);

GLfloat spin = 0.0;
GLfloat spin_incr = 1.0;

// Text
GLvoid *font_style = GLUT_BITMAP_HELVETICA_12;

// OpenGL Function Prototypes
void init();
void display();
void reshape(int width, int height);
void update();

// Other fuctions
void printText(void);
void printString(float x, float y, float z, const char* format, ...);

void init()
{
	// Background is set to Black
	glClearColor(0.0, 0.0, 0.0, 1.0);
	newBody.LoadBodies();
	//glColor3f(1.0, 1.0, 1.0);
	//glShadeModel(GL_FLAT);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	printText();

	glColor4f(0.5, 0.5, 0.0, 1.0);
	glLineWidth(2.0);
	newScene.DrawAxes();
	glColor4f(0.5, 0.5, 0.0, 0.5);
	glLineWidth(1.0);
	newScene.DrawGrid();

	// Center of world
	glColor3f(0.0, 0.0, 1.0);
	glBegin(GL_POINTS);
	glVertex2f(0.0, 0.0);
	glEnd();


	//glLineWidth(1.0);
	// Arm Base
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);
	glVertex2f(-14, -100);
	glVertex2f(-14, -86);
	glVertex2f(14, -86);
	glVertex2f(14, -100);
	glEnd();

	// Arm base center
	glColor3f(1.0, 0.0, 0.0);
	glPointSize(10.0);
	glBegin(GL_POINTS);
	glVertex2f(0, -93);
	glEnd();

	glLineWidth(5.0);

	// Target Coordinates
	glBegin(GL_POINTS);
		glVertex2f(target.GetX(), target.GetY());
	glEnd();

	
	newBody.DrawBodies();

	// End Effector Vector
	float u[2];
	// Target vector
	float v[2];
	// Result vector
	float r[2];

	// 
	float angle = 0;
	float sign = 0;
	const float epsilon = 0.0001;

	float newSX = 0;
	float newSY = 0;
	float newEX = 0;
	float newEY = 0;
	GLfloat mat[16];

	do
	{
		//if (frameNumber % 200 == 0)
		//{
			for (int i = newBody.size - 1; i >= 0; i--)
			{
				//printText();
				// End Effector Vector
				// The origin is the current link to evaluate but the end is the end of the max link in the body
				u[0] = newBody.bodies[newBody.size - 1].GetEnd().GetX() - newBody.bodies[i].GetStart().GetX();
				u[1] = newBody.bodies[newBody.size - 1].GetEnd().GetY() - newBody.bodies[i].GetStart().GetY();

				// Target Vector
				v[0] = target.GetX() - newBody.bodies[i].GetStart().GetX();
				v[1] = target.GetY() - newBody.bodies[i].GetStart().GetY();

				angle = space2D.AngleBetweenVectors(u, v);
				sign = space2D.CrossProduct(u, v);

				// Remove the sign of angle
				angle = sqrt(angle * angle);

				if (sign < 0) angle = angle * -1;

				glPushMatrix();
				glTranslatef(newBody.bodies[i].GetStart().GetX(), newBody.bodies[i].GetStart().GetY(), 0.0);
				glRotatef(angle, 0.0, 0.0, 1.0);
				glTranslatef((-1)*newBody.bodies[i].GetStart().GetX(), (-1)*newBody.bodies[i].GetStart().GetY(), 0.0);

				for (int j = i; j <= newBody.size - 1; j++)
				//for (int j = i; j <= i; j++)
				{
					glColor3f(1.0, 1.0, 0.0);
					glBegin(GL_POINTS);
					glColor3f(0.0, 0.5, 0.0);
					glVertex2f(newBody.bodies[j].GetEnd().GetX(), newBody.bodies[j].GetEnd().GetY());
					glColor3f(0.0, 1.0, 0.0);
					glVertex2f(newBody.bodies[j].GetStart().GetX(), newBody.bodies[j].GetStart().GetY());
					glEnd();

					glBegin(GL_LINES);
					glVertex2f(newBody.bodies[j].GetStart().GetX(), newBody.bodies[j].GetStart().GetY());
					glVertex2f(newBody.bodies[j].GetEnd().GetX(), newBody.bodies[j].GetEnd().GetY());
					glEnd();
				}

				/*glColor3f(1.0, 1.0, 0.0);
				glBegin(GL_POINTS);
				glColor3f(0.0, 0.5, 0.0);
				glVertex2f(newBody.bodies[i].GetEnd().GetX(), newBody.bodies[i].GetEnd().GetY());
				glColor3f(0.0, 1.0, 0.0);
				glVertex2f(newBody.bodies[i].GetStart().GetX(), newBody.bodies[i].GetStart().GetY());
				glEnd();

				glBegin(GL_LINES);
				glVertex2f(newBody.bodies[i].GetStart().GetX(), newBody.bodies[i].GetStart().GetY());
				glVertex2f(newBody.bodies[i].GetEnd().GetX(), newBody.bodies[i].GetEnd().GetY());
				glEnd();*/

				/*newSX = newBody.bodies[i].GetStart().GetX() * cos(angle) - newBody.bodies[i].GetStart().GetY() * sin(angle);
				newSY = newBody.bodies[i].GetStart().GetY() * sin(angle) + newBody.bodies[i].GetStart().GetX() * cos(angle);

				newEX = newBody.bodies[i].GetEnd().GetX() * cos(angle) - newBody.bodies[i].GetEnd().GetY() * sin(angle);
				newEY = newBody.bodies[i].GetEnd().GetY() * sin(angle) + newBody.bodies[i].GetEnd().GetX() * cos(angle);

				glBegin(GL_POINTS);
				glColor3f(1.0, 0.0, 1.0);
				glVertex2f(newSX, newSY);
				glVertex2f(newEX, newEY);
				glEnd();*/
				glGetFloatv(GL_MODELVIEW_MATRIX, mat);
				glPopMatrix();


				for (int j = i; j <= newBody.size - 1; j++)
				{
					float start[3];
					float end[3];

					start[0] = mat[0] * newBody.bodies[j].GetStart().GetX() + mat[4] * newBody.bodies[j].GetStart().GetY() + mat[8] * 1 + mat[12];
					start[1] = mat[1] * newBody.bodies[j].GetStart().GetX() + mat[5] * newBody.bodies[j].GetStart().GetY() + mat[9] * 1 + mat[13];
					start[2] = mat[2] * newBody.bodies[j].GetStart().GetX() + mat[6] * newBody.bodies[j].GetStart().GetY() + mat[10] * 1 + mat[14];

					end[0] = mat[0] * newBody.bodies[j].GetEnd().GetX() + mat[4] * newBody.bodies[j].GetEnd().GetY() + mat[8] * 1 + mat[12];
					end[1] = mat[1] * newBody.bodies[j].GetEnd().GetX() + mat[5] * newBody.bodies[j].GetEnd().GetY() + mat[9] * 1 + mat[13];
					end[2] = mat[2] * newBody.bodies[j].GetEnd().GetX() + mat[6] * newBody.bodies[j].GetEnd().GetY() + mat[10] * 1 + mat[14];

					Point2D newStart;
					Point2D newEnd;
					newStart.SetX(start[0]);
					newStart.SetY(start[1]);

					newEnd.SetX(end[0]);
					newEnd.SetY(end[1]);

					newBody.bodies[j].SetStart(newStart);
					newBody.bodies[j].SetEnd(newEnd);

					glBegin(GL_POINTS);
					glColor3f(1.0, 0.5, 0.2);
					glVertex2f(start[0], start[1]);
					glVertex2f(end[0], end[1]);
					glEnd();
				}

				/*float start[3];
				float end[3];

				start[0] = mat[0] * newBody.bodies[i].GetStart().GetX() + mat[4] * newBody.bodies[i].GetStart().GetY() + mat[8] * 1 + mat[12];
				start[1] = mat[1] * newBody.bodies[i].GetStart().GetX() + mat[5] * newBody.bodies[i].GetStart().GetY() + mat[9] * 1 + mat[13];
				start[2] = mat[2] * newBody.bodies[i].GetStart().GetX() + mat[6] * newBody.bodies[i].GetStart().GetY() + mat[10] * 1 + mat[14];

				end[0] = mat[0] * newBody.bodies[i].GetEnd().GetX() + mat[4] * newBody.bodies[i].GetEnd().GetY() + mat[8] * 1 + mat[12];
				end[1] = mat[1] * newBody.bodies[i].GetEnd().GetX() + mat[5] * newBody.bodies[i].GetEnd().GetY() + mat[9] * 1 + mat[13];
				end[2] = mat[2] * newBody.bodies[i].GetEnd().GetX() + mat[6] * newBody.bodies[i].GetEnd().GetY() + mat[10] * 1 + mat[14];

				Point2D newStart;
				Point2D newEnd;
				newStart.SetX(start[0]);
				newStart.SetY(start[1]);
				
				newEnd.SetX(end[0]);
				newEnd.SetY(end[1]);

				newBody.bodies[i].SetStart(newStart);
				newBody.bodies[i].SetEnd(newEnd);

				glBegin(GL_POINTS);
				glColor3f(1.0, 0.5, 0.2);
				glVertex2f(start[0], start[1]);
				glVertex2f(end[0], end[1]);
				glEnd();*/

				space2D.VectorSubstraction(u, v, r);
			}
		//}
	} while (space2D.Norm(r) <= epsilon);



	/// --- Isolated case
	/*
	float newUX = newBody.bodies[2].GetEnd().GetX() - newBody.bodies[2].GetStart().GetX();
	float newUY = newBody.bodies[2].GetEnd().GetY() - newBody.bodies[2].GetStart().GetY();

	float targetX = target.GetX() - newBody.bodies[2].GetStart().GetX();
	float targetY = target.GetY() - newBody.bodies[2].GetStart().GetY();
	
	float u[2] = { newUX, newUY };
	float v[2] = { targetX, targetY };

	float angle = space2D.AngleBetweenVectors(u, v);
	

	float sign = space2D.CrossProduct(u, v);
	if (sign >= 0) angle = sqrt(angle * angle);
	else angle = sqrt(angle * angle) * -1;
	std::cout << "Angle: " << angle << std::endl;
	std::cout << "Sign: " << sign << std::endl;
	
	glPushMatrix();
	glTranslatef(newBody.bodies[2].GetStart().GetX(), newBody.bodies[2].GetStart().GetY(), 0.0);
	glRotatef(angle, 0.0, 0.0, 1.0);
	glTranslatef((-1)*newBody.bodies[2].GetStart().GetX(), (-1)*newBody.bodies[2].GetStart().GetY(), 0.0);
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_POINTS);
		glColor3f(0.0, 0.5, 0.0);
		glVertex2f(newBody.bodies[2].GetEnd().GetX(), newBody.bodies[2].GetEnd().GetY());
		glColor3f(0.0, 1.0, 0.0);
		glVertex2f(newBody.bodies[2].GetStart().GetX(), newBody.bodies[2].GetStart().GetY());
	glEnd();
	glBegin(GL_LINES);
	glVertex2f(newBody.bodies[2].GetStart().GetX(), newBody.bodies[2].GetStart().GetY());
	glVertex2f(newBody.bodies[2].GetEnd().GetX(), newBody.bodies[2].GetEnd().GetY());
	glEnd();

	glPopMatrix();
	*/
	/// ---

	/*float e = 0;
	do
	{
		for (int i = 3; i >= 0; i--)
		{
			float newUX = newBody.bodies[3].GetEnd().GetX() - newBody.bodies[]
			float u[2] = {newBody.bodies[3].GetEnd().GetX() - };
		}
	} while( e > 1);*/


	glFlush();
	glutSwapBuffers();

	/*spin += spin_incr;
	if (spin > 360.0)spin -= 360.0;*/
	frameNumber++;
}

void reshape(int width, int height)
{
	float aspectRatio = 0;

	screenWidth = width;
	screenHeight = height;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//gluOrtho2D(0, screenWidth, 0, screenHeight);

	if (width == 0) width = 1;
	if (height == 0) height = 1;

	if (width <= height)
	{
		aspectRatio = (float)width / (float)height;
		gluOrtho2D(left, right, bottom * aspectRatio, top * aspectRatio);
	}
	else
	{
		aspectRatio = (float)height/ (float)width;
		gluOrtho2D(left * aspectRatio, right * aspectRatio, bottom, top);
	}


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void update()
{
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(screenWidth, screenHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Inverse Kinematics CCD Example");

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	init();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(update);

	glutMainLoop();

	return 0;
}

void printText(void)
{
	char buffer[10];

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	glMatrixMode(GL_MODELVIEW);

	memset(&buffer[0], 0, sizeof(buffer));

	glColor4f(0.5, 0.5, 0.5, 0.5);
	// First Smoke color rectangle
	glBegin(GL_POLYGON);
	glVertex3f(0.5, 98, 1);
	glVertex3f(45, 98, 1);
	glVertex3f(45, 73, 1);
	glVertex3f(0.5, 73, 1);
	glEnd();


	//Instructions
	glColor4f(1.0, 1.0, 1.0, 1.0);
	printString(1, 96, 1, "%s", "Body Data:");

	printString(10, 96, 1, "%s", "Bone0 Start - X:");
	_itoa_s(newBody.bodies[0].GetStart().GetX(), buffer, 10);
	printString(25, 96, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 94, 1, "%s", "Bone0 Start - Y:");
	_itoa_s(newBody.bodies[0].GetStart().GetY(), buffer, 10);
	printString(25, 94, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 92, 1, "%s", "Bone0 End - X:");
	_itoa_s(newBody.bodies[0].GetEnd().GetX(), buffer, 10);
	printString(25, 92, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 90, 1, "%s", "Bone0 End - Y:");
	_itoa_s(newBody.bodies[0].GetEnd().GetY(), buffer, 10);
	printString(25, 90, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 88, 1, "%s", "Bone1 Start - X:");
	_itoa_s(newBody.bodies[1].GetStart().GetX(), buffer, 10);
	printString(25, 88, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 86, 1, "%s", "Bone1 Start - Y:");
	_itoa_s(newBody.bodies[1].GetStart().GetY(), buffer, 10);
	printString(25, 86, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 84, 1, "%s", "Bone1 End - X:");
	_itoa_s(newBody.bodies[1].GetEnd().GetX(), buffer, 10);
	printString(25, 84, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 82, 1, "%s", "Bone1 End - Y:");
	_itoa_s(newBody.bodies[1].GetEnd().GetY(), buffer, 10);
	printString(25, 82, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	printString(10, 80, 1, "%s", "Bone2 Start - X:");
	_itoa_s(newBody.bodies[2].GetStart().GetX(), buffer, 10);
	printString(25, 80, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 78, 1, "%s", "Bone2 Start - Y:");
	_itoa_s(newBody.bodies[2].GetStart().GetY(), buffer, 10);
	printString(25, 78, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	
	printString(10, 76, 1, "%s", "Bone2 End - X:");
	_itoa_s(newBody.bodies[2].GetEnd().GetX(), buffer, 10);
	printString(25, 76, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));
	printString(10, 74, 1, "%s", "Bone2 End - Y:");
	_itoa_s(newBody.bodies[2].GetEnd().GetY(), buffer, 10);
	printString(25, 74, 1, buffer, 1.0, 0.0, 0.0, 1.0);
	memset(&buffer[0], 0, sizeof(buffer));

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	//glDisable(GL_BLEND);
}

void printString(float x, float y, float z,const char* format, ...)
{
	va_list args;
	int len;
	int i;
	char * text;

	va_start(args, format);
	len = _vscprintf(format, args) + 1;
	text = (char*)malloc(len * sizeof(char));
	vsprintf_s(text, len, format, args);
	va_end(args);
	glRasterPos3f(x, y, z);
	for (i = 0; text[i] != '\0'; i++)
	{
		glutBitmapCharacter(font_style, text[i]);
	}
	free(text);

}