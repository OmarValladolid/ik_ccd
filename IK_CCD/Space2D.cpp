#include "Space2D.h"
#include <iostream>
#include <math.h>

/// Constant values
#define PI 3.14159265

/// <summary>
/// Space2D constructor
/// </summary>
Space2D::Space2D()
{
}

/// <summary>
/// Space2D destructor
/// </summary>
Space2D::~Space2D()
{
}

/// <summary>
/// Vector addition
/// </summary>
/// <param name="u">Vector u</param>
/// <param name="v">Vector v</param>
/// <param name="result">Result of the vector addtion</param>
void Space2D::VectorAddition(float u[2], float v[2], float result[2])
{
	result[0] = u[0] + v[0];
	result[1] = u[1] + v[1];
}

/// <summary>
/// Vector subtraction
/// </summary>
/// <param name="u">Vector u</param>
/// <param name="v">Vector v</param>
/// <param name="result">Result of the vector subtraction</param>
void Space2D::VectorSubstraction(float u[2], float v[2], float result[2])
{
	result[0] = u[0] - v[0];
	result[1] = u[1] - v[1];
}

/// <summary>
/// Get the angle between a vector and the negative vertigal
/// </summary>
/// <param name="u">2D Vector</param>
/// <returns>Angle respect the negative vertical</returns>
float Space2D::VecAngleRespectNVertical(float u[2])
{
	float angle;
	float x_opposite = std::abs(u[0]);
	float y_adyacent = std::abs(u[1]);

	// to do: Validate  quadrants conditions
	if ((x_opposite == 0 && y_adyacent == 0) || (x_opposite == 0 && y_adyacent > 0))
		return 0;

	int direction = AngleDirectionRespectVertical(u);

	angle = atan(x_opposite / y_adyacent) * (180 / PI);
	return (angle * direction);
}

/// <summary>
/// Get the angle direction respect the negative vertical (-y axis)
/// </summary>
/// <param name="u">2D Vector</param>
/// <returns>Angle direction</returns>
int Space2D::AngleDirectionRespectVertical(float u[2])
{
	float x = u[0];
	float y = u[1];

	if (x > 0 && y > 0)
		return 1;
	if (x > 0 && y < 0)
		return 1;
	if (x < 0 && y < 0)
		return -1;
	if (x < 0 && y > 0)
		return -1;
}

/// <summary>
/// Dot Product between 2 Points (x, y)
/// </summary>
/// <returns>Dot product result</returns>
float Space2D::DotProduct(float pointA[2], float pointB[2])
{
	float result = 0;

	result = (pointA[0] * pointB[0]) + (pointA[1] * pointB[1]);


	return result;
}

/// <summary>
/// Square Norm
/// </summary>
/// <returns>Square norm result</returns>
float Space2D::SquareNorm(float pointA[2])
{
	return (pointA[0] * pointA[0]) + (pointA[1] * pointA[1]);
}

/// <summary>
/// Norm of a Vector (Magnitude)
/// </summary>
/// <returns>Norm result</returns>
float Space2D::Norm(float pointA[2])
{
	return sqrt(SquareNorm(pointA));
}

/// <summary>
/// Calculates the distance between two vectors
/// </summary>
/// <param name="u">Vector u</param>
/// <param name="v">Vector v</param>
/// <returns>Distance result</returns>
float Space2D::DistanceBetweenVectors(float u[2], float v[2])
{
	float result = 0;

	result = sqrt(((v[0] - u[0]) * (v[0] - u[0])) + ((v[1] - u[1]) * (v[1] - u[1])));

	return result;
}

/// <summary>
/// Calculates the angle between two vectors
/// </summary>
/// <param name="u">Vector u</param>
/// <param name="v">Vector v</param>
/// <returns>Angle between two vectors</returns>
float Space2D::AngleBetweenVectors(float u[2], float v[2])
{
	float angle = 0;
	float uDotv, uNorm, vNorm, uNormXvNorm;

	uDotv = DotProduct(u, v);
	uNorm = (float)Norm(u);
	vNorm = (float)Norm(v);

	uNormXvNorm = uDotv / (uNorm * vNorm);// uNorm * vNorm;

	if (uNormXvNorm < 1.0)
		angle = (float)(acosf(uNormXvNorm) * (180.0 / PI));
	else
		angle = 0.0;

	return angle;
}

/// <summary>
/// Calculates the Cross Product between two vectors
/// </summary>
/// <param name="u">Vector u</param>
/// <param name="v">Vector v</param>
/// <returns>Cross product result</returns>
float Space2D::CrossProduct(float u[2], float v[2])
{
	// u x v = det(u v) = (u_x * v_y) - (u_y * v_x)
	float result = 0;
	result = (u[0] * v[1]) - (u[1] * v[0]);

	return result;
}