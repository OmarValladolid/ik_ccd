#pragma once
#include "Point2D.h"

/// <summary>
/// LineObj class
/// </summary>
class LineObj
{
private:
	Point2D _start;
	Point2D _end;
	float _lineColor[4];
	float _pointColor[4];
public:
	/// <summary>
	/// LineObj constructor
	/// </summary>
	LineObj(void);

	/// <summary>
	/// LineObj constructor
	/// </summary>
	/// <param name="start">Start point</param>
	/// <param name="end">End point</param>
	/// <param name="lineColor">Line color</param>
	/// <param name="pointColor">Point color for line vertices</param>
	LineObj(Point2D start, Point2D end, float lineColor[4], float pointColor[4]);
	
	/// <summary>
	/// LineObj destructor
	/// </summary>
	~LineObj();

	/// <summary>
	/// Set the start point
	/// </summary>
	void SetStart(Point2D s);
	
	/// <summary>
	/// Set the end point
	/// </summary>
	void SetEnd(Point2D e);

	/// <summary>
	/// Set the color of the line (RGBA)
	/// </summary>
	void SetLineColor(float color[4]);
	
	/// <summary>
	/// Set the color of the line vertices (RGBA)
	/// </summary>
	void SetPointColor(float color[4]);

	/// <summary>
	/// Draw line
	/// </summary>
	void DrawLine(void);

	/// <summary>
	/// Draw line vertices
	/// </summary>
	void DrawPoints(void);

	/// <summary>
	/// Set the color (RGBA) to be used in OpenGL
	/// </summary>
	void SetColor(float color[4]);

	/// <summary>
	/// Get the start point
	/// </summary>
	/// <returns>Start point object</returns>
	Point2D GetStart(void);
	
	/// <summary>
	/// Get the end point
	/// </summary>
	/// <returns>End point object</returns>
	Point2D GetEnd(void);
	
};