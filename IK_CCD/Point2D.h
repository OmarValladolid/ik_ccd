#pragma once

/// <summary>
/// Point2D class
/// </summary>
class Point2D
{
private:
	float _x;
	float _y;
public:

	/// <summary>
	/// Point2D constructor
	/// </summary>
	Point2D(void);

	/// <summary>
	/// Point2D constructor
	/// </summary>
	/// <param name="x">x value</param>
	/// <param name="y">y value</param>
	Point2D(float x, float y);

	/// <summary>
	/// Point2D destructor
	/// </summary>
	~Point2D();

	/// <summary>
	/// Set the value of x
	/// </summary>
	/// <param name="x">x value</param>
	void SetX(float x);

	/// <summary>
	/// Set the value of y
	/// </summary>
	/// <param name="x">y value</param>
	void SetY(float y);

	/// <summary>
	/// Set the value of x and y
	/// </summary>
	/// <param name="points">Point in 2D</param>
	void SetPoints(Point2D points);

	/// <summary>
	/// Get the value of x
	/// </summary>
	/// <returns>x value</returns>
	float GetX(void);

	/// <summary>
	/// Get the value of y
	/// </summary>
	/// <returns>y value</returns>
	float GetY(void);
};

