#pragma once

/// <summary>
/// Space2D Class
/// </summary>
class Space2D
{
public:
	/// <summary>
	/// Space2D constructor
	/// </summary>
	Space2D();

	/// <summary>
	/// Space2D destructor
	/// </summary>
	~Space2D();

	/// <summary>
	/// Vector addition
	/// </summary>
	/// <param name="u">Vector u</param>
	/// <param name="v">Vector v</param>
	/// <param name="result">Result of the vector addtion</param>
	void VectorAddition(float u[2], float v[2], float result[2]);

	/// <summary>
	/// Vector subtraction
	/// </summary>
	/// <param name="u">Vector u</param>
	/// <param name="v">Vector v</param>
	/// <param name="result">Result of the vector subtraction</param>
	void VectorSubstraction(float u[2], float v[2], float result[2]);

	/// <summary>
	/// Get the angle between a vector and the negative vertigal
	/// </summary>
	/// <param name="u">2D Vector</param>
	/// <returns>Angle respect the negative vertical</returns>
	float VecAngleRespectNVertical(float u[2]);

	/// <summary>
	/// Get the angle direction
	/// </summary>
	/// <param name="u">2D Vector</param>
	/// <returns>Angle direction</returns>
	int AngleDirectionRespectVertical(float u[2]);

	/// <summary>
	/// Dot Product between 2 Points (x, y)
	/// </summary>
	/// <returns>Dot product result</returns>
	float DotProduct(float pointA[2], float pointB[2]);

	/// <summary>
	/// Square Norm
	/// </summary>
	/// <returns>Square norm result</returns>
	float SquareNorm(float pointA[2]);

	/// <summary>
	/// Norm of a Vector (Magnitude)
	/// </summary>
	/// <returns>Norm result</returns>
	float Norm(float pointA[2]);

	/// <summary>
	/// Calculates the distance between two vectors
	/// </summary>
	/// <param name="u">Vector u</param>
	/// <param name="v">Vector v</param>
	/// <returns>Distance result</returns>
	float DistanceBetweenVectors(float u[2], float v[2]);

	/// <summary>
	/// Calculates the angle between two vectors
	/// </summary>
	/// <param name="u">Vector u</param>
	/// <param name="v">Vector v</param>
	/// <returns>Angle between two vectors</returns>
	float AngleBetweenVectors(float u[2], float v[2]);

	/// <summary>
	/// Calculates the Cross Product between two vectors
	/// </summary>
	/// <param name="u">Vector u</param>
	/// <param name="v">Vector v</param>
	/// <returns>Cross product result</returns>
	float CrossProduct(float u[2], float v[2]);

};

